package com.sky.controller.admin;

import com.sky.result.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

@RestController("adminShopController")
@RequestMapping("/admin/shop")
@Slf4j
@Api(tags = "管理端商家状态相关接口")
public class ShopController {

    private static final String SHOPSTATUS= "SHOP_STATUS";

    @Autowired
    RedisTemplate redisTemplate;

    @PutMapping("/{status}")
    @ApiOperation("设置商家状态")
    public Result setStatus(@PathVariable Integer status){
        //1.接收参数
        log.info("设置商家状态：{}",status==1?"营业中":"打烊");
        //2.操作redis，设置营业状态
        redisTemplate.opsForValue().set(SHOPSTATUS,status);

        //3.做出响应
        return Result.success();
    }

    @GetMapping("/status")
    @ApiOperation("获取商家状态")
    public Result<Integer> getStatus(){
        //从redis,获取营业状态
        Integer shop_status = (Integer) redisTemplate.opsForValue().get(SHOPSTATUS);
        //3.做出响应
        return Result.success(shop_status);
    }
}
